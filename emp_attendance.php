<?php
$j = 0;

require_once 'inc/core/init.php';


$user = new CHRM_User();
if($user->isLoggedIn())
{
    if($user->hasPermission('admin')) {
        CHRM_Redirect::to('dashboard.php');
    }
    if(!$user->hasPermission('admin'))
    {

    }
}

if (!empty($_SESSION['username'])) {
    header('Location: employees.php');
    echo 'This Worked!';
    exit;
}

$sessionName = CHRM_Config::get('session/session_name');
if(!CHRM_Session::exists($sessionName)) {
    CHRM_Redirect::to('index.php');
}


////////////////////////////////////////INSERT VALUES BEGINS HERE ///////////////////////////////////////////////////

if(isset($_POST['add']))
{
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conn = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerify = 'display-hide'; //for the popup to show that update has been sucessful

    if(get_magic_quotes_gpc()){
        $emp_pkid['pkid'];
        $emp_id = $_POST['id'];
        $emp_timein = $_POST['timein'];
        $emp_timeout = $_POST['timeout'];
        $emp_note = $_POST['note'];

    } else {
        $emp_pkid['pkid'];
        $emp_id = $_POST['id'];
        $emp_timein = $_POST['timein'];
        $emp_timeout = $_POST['timeout'];
        $emp_note = $_POST['note'];

    }


    $sql = "INSERT INTO monitor_attendance ".
        "(id,timein,timeout,note)".
        "VALUES('$emp_id','$emp_timein' ,'$emp_timeout','$emp_note')" ;

    mysql_select_db('coffeehrm');

    $retval = mysql_query( $sql, $conn );
    if(! $retval )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conn);

}


//////////////////////////////////////INSERT VALUES ENDS HERE ////////////////////////////////////////////////////////


/////////////////////////////////FIRST PORTLET UPDATE START //////////////////////////////////////////////////////////
if(isset($_POST['update']))
{
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conn = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerify = 'display-hide'; //for the popup to show that update has been sucessful

    $emp_pkid = $_POST['pkid'];
    $emp_id = $_POST['id'];
    $emp_timein = $_POST['timein'];
    $emp_timeout = $_POST['timeout'];
    $emp_note = $_POST['note'];

    $sql = "UPDATE monitor_attendance ".
        "SET id = '$emp_id', timeout = '$emp_timeout', note = '$emp_note'".
        "WHERE pkid = '$emp_pkid'" ;

    mysql_select_db('coffeehrm');

    $retval = mysql_query( $sql, $conn );
    if(! $retval )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conn);

}
///////////////////////////////////////////////////FIRST PORTLET UPDATE END /////////////////////////////////////////////

//////////////////////////////////////////////////////FIRST PORTLET DELETE START/////////////////////////////////////////
if(isset($_POST['delete'])) {
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conndel1 = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conndel1 )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerifydel = 'display-hide'; //for the popup to show that update has been sucessful

    $emp_pkid = $_POST['pkid'];
    $emp_id = $_POST['id'];
    $emp_timein = $_POST['timein'];
    $emp_timeout = $_POST['timeout'];
    $emp_note = $_POST['note'];

    $sqldel = "DELETE FROM monitor_attendance ".
        "WHERE pkid = $emp_pkid " ;

    mysql_select_db('coffeehrm');

    $retvaldel = mysql_query( $sqldel, $conndel1 );
    if(! $retvaldel )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conndel1);

}
////////////////////////////////////////////////////FIRST PORTLET DELETE END /////////////////////////////////////////////



?>

<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>CoffeeHRM | Employees Leave Application</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="Osama Iqbal" name="author"/>




    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->






    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <![endif]-->


    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/tableExport.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/scripts/core/app.js"></script>
    <script src="assets/scripts/custom/table-editable.js"></script>



    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>


    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="css/style-coffeehrm.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">

<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="dashboard.php">
            <img src="assets/img/logo.png" alt="logo" class="img-responsive"/>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">


            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username">
						 Welcome
					</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">


            </li>
            <li>
                <a href="javascript:;" id="trigger_fullscreen">
                    <i class="fa fa-arrows"></i> Full Screen
                </a>
            </li>
            <li class="divider">
            <li>
                <a href="logout.php">
                    <i class="fa fa-key"></i> Log Out
                </a>
            </li>
        </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>

            <li class = "start">
                <a href="emp_details.php">
                    <i class="fa fa-book"></i>
						<span class="title">
							Deatils
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
            <li>
                <a href="emp_leaves.php">
                    <i class="fa fa-calendar"></i>
						<span class="title">
							Leave Application
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li class ="last">
                <a href="emp_attendance.php">
                    <i class="fa fa-clock-o"></i>
						<span class="title">
							Register Attendance
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Attendance <small>Update Leave Details</small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">

            <li>
                <i class="fa fa-home"></i>
                <a href="dashboard.php">
                    Home
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">
                    Admin
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="emp_attendance.php">
                    Attendance
                </a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">

<div class="portlet box purple">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Attendance Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">
    <div class="btn-group">
        <a href="#portlet-configadd" data-toggle="modal" class="config">
            <button class="btn yellow">
                Add New <i class="fa fa-plus"></i>
            </button>
        </a>
    </div>

    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_1').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_1').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
<tr>
    <th>Employee ID</th>
    <th>
        Time In
    </th>
    <th>
        Time Out
    </th>
    <th>
        Note
    </th>

    <th>
        Edit
    </th>
    <th>
        Delete
    </th>
</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$katka = $user->data()->empid;
$query = mysql_query("select * from monitor_attendance where id = $katka");
//$query2 = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {



    ?>

    <tr>
    <td><?php echo $row['id']; ?></td>
    <td><?php echo $row['timein']?></td>
    <td><?php echo $row['timeout']?></td>
    <td><?php echo $row['note']?></td>

    <td><div id="thanks"><a class = "" data-toggle="modal" href = "#portlet-config<?php echo $i; ?>" data-id = '"<?php echo $row['id']; ?>"' data-empno ='"<?php echo $row['empno']; ?>"' data-firstname = '"<?php echo $row['firstname']; ?>"' data-lastname ='"<?php echo $row['lastname']; ?>"'>
                Edit
            </a></div>
        <script>
            $(document).ready(function(){

                $('a.edit').click(function(){
                    var id = $(this).data('id');
                    var empno = $(this).data('empno');
                    var firstname = $(this).data('firstname');
                    var middlename = $(this).data('middlename');
                    var lastname = $(this).data('lastname');

                    $('#myid').val(id);
                    $('#myempno').val(empno);
                    $('#myfirstname').val(firstname);
                    $('#mymiddlename').val(middlename)
                    $('#mylastname').val(lastname);

                });

            });
        </script>

        <!-- YOUR MODAL GOES HERE  -->
        <div class="modal fade" id="portlet-config<?php echo $i;?>"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-wide modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN VALIDATION STATES-->

                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form id="form_sample_1" class="form-horizontal emp1" name="emp1" action="" method="post">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have not been updated.You have some form errors. Please check below.
                                            </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have been updated successfully.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Employee ID
										<span class="required">
											 *
										</span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type = "hidden" name = "pkid" value = "<?php echo $row['pkid'];?>"
                                                    <input type = "hidden" name = "id" value = "<?php echo $row['id']; ?>">

                                                    <input type = "hidden" name = "timeout" value = "<?php echo $row['timeout']; ?>">
                                                    <input type = "hidden" name = "note" value = "<?php echo $row['note']; ?>">



                                                    <input required readonly  type="text" name="id" data-required="1" autocomplete="off" class="form-control input-large" value="<?php echo $row['id']; ?>"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Time Out
										<span class="required">
											 *
										</span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input required  type="datetime-local" id="abc" name="timeout" data-required="1" autocomplete="off" class="form-control input-large" value="<?php echo $row['timeout']; ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Note
										<span class="required">
											 *
										</span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input required type="text" name="note" data-required="1" autocomplete="off" class="form-control input-large" value="<?php echo $row['note']; ?>"/>
                                                </div>
                                            </div>



                                            <div class="modal-footer form-actions fluid">
                                                <input id="update" name="update" type="submit" class="btn btn-success" value="Save Changes" onclick="setTimeout(ajaxCall(),2000)">
                                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            </div>
                                    </form>

                                    <!-- END FORM-->
                                </div>

                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div></div>
        <script>
            function ajaxCall(){
                $(document).ready(function () {
                    $("input#update").click(function(e){

                        $.ajax({
                            type: "POST",
                            url: "employees.php", //
                            data: $('form.emp1').serialize(), //class of the form
                            success: function(msg){
                                //$("#thanks").html(msg);

                                //alert($('#abc').val());
                                $("#portlet-config<?php echo $i; ?>").modal('hide');	// modal id
                            },
                            error: function(){
                                alert("failure");
                            }
                        });
                    });
                });
            }
        </script>
    </td>
    <td><a class = "" data-toggle="modal" href = "#portlet-configdel<?php echo $i; ?>" data-id = '"<?php echo $row['id']; ?>"' data-empno ='"<?php echo $row['empno']; ?>"' data-firstname = '"<?php echo $row['firstname']; ?>"' data-lastname ='"<?php echo $row['lastname']; ?>"'>
            Delete
        </a>
        <script>
            $(document).ready(function(){

                $('a.edit').click(function(){
                    var id = $(this).data('id');
                    var empno = $(this).data('empno');
                    var firstname = $(this).data('firstname');
                    var lastname = $(this).data('lastname');

                    $('#myid').val(id);
                    $('#myempno').val(empno);
                    $('#myfirstname').val(firstname);
                    $('#mylastname').val(lastname);
                });

            });
        </script>
        <div class="modal fade" id="portlet-configdel<?php echo $i;?>"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-wide modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form id="form_sample_1" class="form-horizontal emp1del" name="emp1" action="" method="post">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have not been updated.You have some form errors. Please check below.
                                            </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have been updated successfully.
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger">
                                                    <button class="close" data-close="alert"></button>
                                                    Are you sure you want to delete details?
                                                    <input type = "hidden" name = "pkid" value = "<?php echo $row['pkid'];?>"
                                                    <input type = "hidden" name = "id" value = "<?php echo $row['id']; ?>">
                                                    <input type = "hidden" name = "timein" value = "<?php echo $row['timein']; ?>">
                                                    <input type = "hidden" name = "timeout" value = "<?php echo $row['timeout']; ?>">
                                                    <input type = "hidden" name = "note" value = "<?php echo $row['note']; ?>">

                                                </div>
                                                <div class="modal-footer form-actions fluid">
                                                    <input id="delete" name="delete" type="submit" class="btn btn-success" value="Delete" onclick="setTimeout(ajaxCall(),2000)">
                                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                                </div>
                                    </form>

                                    <!-- END FORM-->
                                </div>

                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div></div>
        <script>
            function ajaxCall(){
                $(document).ready(function () {
                    $("input#delete").click(function(e){

                        $.ajax({
                            type: "POST",
                            url: "employees.php", //
                            data: $('form.emp1del').serialize(), //class of the form
                            success: function(msg){
                                //$("#thanks").html(msg);

                                //alert($('#abc').val());
                                $("#portlet-configdel<?php echo $i; ?>").modal('hide');	// modal id
                            },
                            error: function(){
                                alert("failure");
                            }
                        });
                    });
                });
            }
        </script>
    </tr>
    <?php $i++;$j++; } ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<script>
    $(document).ready(function(){

        $('a.edit').click(function(){
            var id = $(this).data('id');
            var empno = $(this).data('empno');
            var firstname = $(this).data('firstname');
            var middlename = $(this).data('middlename');
            var lastname = $(this).data('lastname');

            $('#myid').val(id);
            $('#myempno').val(empno);
            $('#myfirstname').val(firstname);
            $('#mymiddlename').val(middlename)
            $('#mylastname').val(lastname);

        });

    });
</script>

<!-- YOUR MODAL GOES HERE  -->
<div class="modal fade" id="portlet-configadd"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-wide modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add New</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN VALIDATION STATES-->

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form id="form_sample_1" class="form-horizontal empadd" name="empadd" action="emp_attendance.php" method="post">
                                <div class="form-body">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button>
                                        Values have not been updated. You have some form errors. Please check below.
                                    </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button>
                                        Values have been added sucessfully.
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Employee ID
										<span class="required">
											 *
										</span>
                                        </label>
                                        <div class="col-md-4">

                                            <input required readonly type="text" name="id" data-required="1" autocomplete="off" class="form-control input-large" value = "<?php echo $user->data()->empid ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Time In
										<span class="required">
											 *
										</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input required type="datetime-local" id="abc" name="timein" data-required="1" autocomplete="off" class="form-control input-large"/>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        </label>

                                        <div class="col-md-4">
                                            <input required type="hidden" id="abc" name="timeout" data-required="1" autocomplete="off" class="form-control input-large"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Note
										<span class="required">
											 *
										</span>
                                        </label>
                                        <div class="col-md-4">
                                            <input required type="text" name="note" data-required="1" autocomplete="off" class="form-control input-large"/>
                                        </div>
                                    </div>

                                </div>



                                <div class="modal-footer form-actions fluid">
                                    <input id="add" name="add" type="submit" class="btn btn-success" value="Save Changes" onclick="setTimeout(ajaxCall(),2000)">
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                </div>
                            </form>

                            <!-- END FORM-->
                        </div>

                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div></div>
<script>
    function ajaxCall(){
        $(document).ready(function () {
            $("input#add").click(function(e){

                $.ajax({
                    type: "POST",
                    url: "employees.php", //
                    data: $('form.empadd').serialize(), //class of the form
                    success: function(msg){
                        //$("#thanks").html(msg);

                        //alert($('#abc').val());
                        $("#portlet-configadd").modal('hide');	// modal id
                    },
                    error: function(){
                        alert("failure");
                    }
                });
            });
        });
    }
</script>
</div>

<!-- END PAGE CONTENT -->
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2014 &copy; CoffeeHRM by Osama Iqbal and Chirag Parasrampuria.

    </div>
    <div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
    </div>
</div>
<!-- END FOOTER -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->


<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>

<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        TableEditable.init();


    });
</script>


<script src="assets/scripts/custom/table-editable.js"></script>



<!--<script>
    $(function(){
        $('#myFormSubmit').click(function(e){
            e.preventDefault();
            alert($('#abc').val());
            /*
             $.post('http://path/to/post',
             $('#myForm').serialize(),
             function(data, status, xhr){
             // do something here with response;
             });
             */
        });
    });
</script> -->

<!-- BEGIN PAGE LEVEL STYLES -->






<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->





<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/tableExport.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/table-editable.js"></script>






<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/form-validation.js"></script>
<!-- END PAGE LEVEL STYLES -->


</body>
<!-- END BODY -->
</html>


