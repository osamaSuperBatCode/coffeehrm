<?php

    //This is a singleton class, and will be instantiated only once, to avoid redundant connections
    class CHRM_DB {

        private static $_instance = null; //$_ is a users way to tell this is kind of private
        private $_pdo, //store the pdo
                $_query, //last query thats executed
                $_error = false, //whether error or not
                $_results, //store the result set
                $_count = 0; //count of results !important

        private function __construct() {

            try {

                $this->_pdo = new PDO('mysql:host='. CHRM_Config::get('mysql/host') . ';dbname=' . CHRM_Config::get('mysql/db'),
                                        CHRM_Config::get('mysql/username'), CHRM_Config::get('mysql/password'));

            } catch (PDOException $e) {

                die($e->getMessage());

            }

        }

        public static function getInstance() {

            if(!isset(self::$_instance)) {
                self::$_instance = new CHRM_DB();
            }
            return self::$_instance;

        }

        public function query($sql, $params = array()) {

            $this->_error = false;
            if($this->_query = $this->_pdo->prepare($sql)) {
                $x = 1;
                if(count($params)) {
                    foreach($params as $param) {
                        $this->_query->bindValue($x, $param);
                        $x++;
                    }
                }

                if($this->_query->execute()) {
                    $this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
                    $this->_count = $this->_query->rowCount();
                } else {
                    $this->_error = true;
                }
            }
            return $this;

        }




        public function action ($action, $table, $where = array()) {

            if(count($where) === 3) {
                $operators = array('=','>','<','>=','<=');

                $field = $where[0];
                $operator = $where[1];
                $value = $where[2];

                if(in_array($operator, $operators)) {
                    $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

                    if(!$this->query($sql, array($value))->error()) {
                        return $this;
                    }
                }
            }
            return false;

        }

        public function get($table, $where) {

            return $this->action('SELECT *', $table, $where);

        }


        public function delete($table, $where) {

            return $this->action('DELETE', $table, $where);

        }

        public function insert($table, $fields = array()) {
            if(count($fields)) {
                $keys = array_keys($fields);
                $values = '';
                $i = 1;

                foreach($fields as $field) {
                    $values .= '?';
                    if($i < count($fields)) {
                        $values .= ', ';
                    }
                    $i++;
                }

                $sql = "INSERT into {$table} (`". implode('`, `', $keys) . "`) VALUES ({$values})";

                if(!$this->query($sql,$fields)->error()) {
                    return true;
                }
            }
            return false;
        }

        public function update($table, $id, $fields) {
            $set = '';
            $i = 1;

            foreach($fields as $name => $value) {
                $set .= "{$name} = ?";


                if($i < count($fields)) {
                    $set .= ', ';
                }
                $i++;
            }

            $sql = "UPDATE {$table} SET {$set} WHERE id = {$id}";

            if(!$this->query($sql, $fields)->error()) {
                return true;
            }
            return false;
        }


        public function results() {
            return $this->_results;
        }

        public function first() {
            return $this->results()[0];
        }

        public function error() {

            return $this->_error;

        }

        public function count() {

            return $this->_count;

        }




    }