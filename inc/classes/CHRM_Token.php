<?php

    class CHRM_Token {

        public static function generate() {
            return CHRM_Session::put(CHRM_Config::get('session/token_name'),md5(uniqid()));
        }

        public static function check($token) {

            $tokenName = CHRM_Config::get('session/token_name');
            if(CHRM_Session::exists($tokenName) && $token === CHRM_Session::get($tokenName)) {
                CHRM_Session::delete($tokenName);
                return true;
            }
            return false;

        }
    }