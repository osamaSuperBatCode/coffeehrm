<?php

    class CHRM_Validation {

        private $_passed = false,
                $_errors = array(),
                $_db = null;


        public function __construct() {

            //$this->_db = new CHRM_DB();
            $this->_db = CHRM_DB::getInstance();

        }

        public function check($source, $items = array()) {

            foreach($items as $item => $rules) {
                foreach( $rules as $rule => $rule_value) {

                    $value = trim($source[$item]);
                    $item = escape($item);

                    if($rule === 'required' && empty($value)) {
                        $this->add_error("{$item} is required");
                    } else if(!empty($value)) {
                        switch($rule) {
                            case 'min':
                                if(strlen($value) < $rule_value) {
                                    $this->add_error("{$item} must be a minimum of {$rule_value} characters.");
                                }
                                break;
                            case 'max':
                                if(strlen($value) > $rule_value) {
                                    $this->add_error("{$item} must be a maximum of {$rule_value} characters.");
                                }
                                break;
                            case 'matches':
                                if($value != $source[$rule_value]) {
                                    $this->add_error("{$rule} value must match {$item}");
                                }
                                break;
                            case 'unique':
                                $check = $this->_db->get($rule_value, array($item, '=' , $value));
                                //$check = $this->_db->count();
                                if($this->$check->count()) {
                                    $this->add_error("{$item} already exists.");
                                }
                                break;
                        }
                    }

                }
            }

            if(empty($this->_errors)) {
                $this->_passed = true;
            }

            return $this;
        }

        private function add_error($error) {
            $this->_errors[] = $error;
        }

        public function errors() {
            return $this->_errors;
        }

        public function passed() {
            return $this->_passed;
        }

    }