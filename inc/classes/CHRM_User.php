<?php


    class CHRM_User {

        private $_db;
        private $_data ,
                $_sessionName,
                $_isLoggedIn,
                $_cookieName;

        public function __construct($user = null) {

            $this->_db = CHRM_DB::getInstance();
           // $dbinstance = $this->_db->insert('users', $fields);

            $this->_sessionName = CHRM_Config::get('session/session_name');
            $this->_cookieName = CHRM_Config::get('remember/cookie_name');

            if(!$user) {

                if(CHRM_Session::exists($this->_sessionName)) {
                    $user = CHRM_Session::get($this->_sessionName);

                    if($this->find($user)) {
                        $this->_isLoggedIn = true;

                    } else {
                        //process logout
                    }

                }

            } else {
                $this->find($user);
            }


        }

        public function update($fields = array(), $id = null) {

            if(!$id && $this->isLoggedIn()) {
                $id = $this->data()->id;
            }

            if(!$this->_db->update('users', $id, $fields)) {
                throw new Exception('There was a problem updating');
            }
        }

        public function create($fields) {
            if(!$this->_db->insert('users', $fields)) {
                throw new Exception('There was a problem creating an account');
            }
        }

        public function find($user = null) {
            if($user) {
                $field = (is_numeric($user)) ? 'id' : 'username';
                $data = $this->_db->get('users',array($field, '=', $user));

                //$datamain = $this->_db;

                if($this->_db->get('users',array($field, '=', $user))->count()) {
                    $this->_data = $this->_db->get('users',array($field, '=', $user))->first();
                    //print_r($this->_data);
                    return true;
                }

            }
            return false;
        }

        public function login($username = null, $password = null, $remember = false) {



            if(!$username && !$password && $this->exists()) {
                CHRM_Session::put($this->_sessionName, $this->data()->id);
            } else {
                $user = $this->find($username);

                if($user) {
                    if($this->data()->password === CHRM_Hash::make($password, $this->data()->salt)) {
                        //print_r($this->_data);
                        CHRM_Session::put($this->_sessionName, $this->data()->id);

                        if($remember) {
                            $hash = CHRM_Hash::unique();
                            $hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->id));

                            if(!$this->_db->get('users_session', array('user_id', '=', $this->data()->id))->count()) {
                                $this->_db->insert('users_session',array(
                                   'user_id' => $this->data()->id,
                                    'hash' => $hash
                                ));
                            } else {
                                $hash = $this->_db->get('users_session', array('user_id', '=', $this->data()->id))->first()->hash;
                            }

                            CHRM_Cookie::put($this->_cookieName, $hash, CHRM_Config::get('remember/cookie_expiry'));
                        }

                        return true;
                    }
                }
            }
            return false;
        }

        public function hasPermission($key) {
            $group = $this->_db->get('groups', array('id', '=', $this->data()->group));
            if(($this->_db->get('groups', array('id', '=', $this->data()->group))->count())) {
                $permissions = json_decode($this->_db->get('groups', array('id', '=', $this->data()->group))->first()->permission, true);
                //print_r($permissions);

                if($permissions[$key] == true) {
                    return true;
                }
            }
            return false;
        }

        public function exists() {
            return (!empty($this->data())) ? true : false;
        }

        public function logout() {

            $this->_db->delete('user_session', array('user_id', '=', $this->data()->id));

            CHRM_Session::delete($this->_sessionName);
            CHRM_Cookie::delete($this->_cookieName);
        }

        public function data() {
            return $this->_data;
        }

        public function isLoggedIn() {
            return $this->_isLoggedIn;
        }

    }