<?php

    //for easier accessing of GLOBALS

    class CHRM_Config {

        public static function get($path = null) {

            if($path) {

                $config = $GLOBALS['config'];
                $path = explode('/', $path);

                foreach ($path as $bit) {
                    if(isset($config[$bit])) {
                        $config = $config[$bit];
                    }
                }
                return $config;
            }
            return false;
        }

    }