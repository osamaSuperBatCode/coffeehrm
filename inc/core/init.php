<?php

    //start the session
    session_start();

    //GLOBAL variable aliases
    $GLOBALS['config'] = array(

        'mysql' => array(
            'host' => '127.0.0.1',
            'username' => 'root',
            'password' => '',
            'db' => 'CoffeeHRM'
        ),
        'remember' => array(
            'cookie_name' => 'hash',
            'cookie_expiry' => 604800
        ),
        'session' => array(
            'session_name' => 'user',
            'token_name' => 'token'
        )

    );

    //autoloading classes from standard php library
    spl_autoload_register(function($class) {

        require_once 'inc/classes/' . $class . '.php';

    });
    // for something like below
    //  $db = new DB();

    //include functions directory, because can't use spl for functions
    require_once __DIR__ . '/functions/CHRM_Sanitize.php';

    //user asked to be remembered
    if(CHRM_Cookie::exists(CHRM_Config::get('remember/cookie_name')) && !CHRM_Session::exists(CHRM_Config::get('session/session_name'))) {
        $hash = CHRM_Cookie::get(CHRM_Config::get('remember/cookie_name'));
        $hashcheck = CHRM_DB::getInstance()->get('users_session', array('hash', '=', $hash));

        if(CHRM_DB::getInstance()->get('users_session', array('hash', '=', $hash))->count()) {
            $user = new CHRM_User(CHRM_DB::getInstance()->get('users_session', array('hash', '=', $hash))->first()->user_id);
            $user->login();

        }
    }