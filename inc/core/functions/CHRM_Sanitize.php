<?php

    //function for escaping strings to avoid sql injections

    function escape($string) {

        return htmlentities($string,ENT_QUOTES,'UTF-8');

    }