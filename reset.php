    <?php
    include('inc/db_mysql.php');
    require_once 'inc/core/init.php';

    if(isset($_GET['action']))
    {
        if($_GET['action']=="reset")
        {
            $encrypt = mysqli_real_escape_string($connection,$_GET['encrypt']);

            $query = "SELECT id FROM users where md5(username)='".$encrypt."'";
            $result = mysqli_query($connection,$query);
            $Results = mysqli_fetch_array($result);
            echo '<br>', $Results['id'];
            if(count($Results)>=1)
            {

            }
            else
            {
                $message = 'Invalid key please try again. <a href="http://localhost/coffeehrm/login.php">Forget Password?</a>';
            }
        }
    }
    elseif(isset($_POST['action']))
    {

        $encrypt      = mysqli_real_escape_string($connection,$_POST['action']);
        $password     = mysqli_real_escape_string($connection,$_POST['password']);
        $query = "SELECT id FROM users where md5(username)='".$encrypt."'";
    //    echo $query;
        $result = mysqli_query($connection,$query);
        $Results = mysqli_fetch_array($result);
        if(count($Results)>=1)
        {
            $salt = CHRM_Hash::salt(32);

            $query = "update users set password='". CHRM_Hash::make(CHRM_Input::get('password2'), $salt) ."' where id='".$Results['id']."'";
            $query2 ="update users set salt='". $salt ."' where id='".$Results['id']."'";
            mysqli_query($connection,$query);
            mysqli_query($connection,$query2);
    //        echo $query;
            $message = "Your password changed sucessfully <a href=\"http://localhost/coffeehrm/login.php/\">click here to login</a>.";
        }
        else
        {
            $message = 'Invalid key please try again. <a href="http://localhost/coffeehrm/login.php">Forget Password?</a>';
        }
    }
    else
    {
        header("location: /login.php");
    }
?>

    <!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Coffee HRM | Enables More Coffee Breaks</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="Coffee HRM" name="description"/>
    <meta content="Osama Iqbal" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <!--<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>-->
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN STYLES -->
    <link href="css/style-coffeehrm.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <!--<link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/> -->
    <!--<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/> -->
    <!--<link href="assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/> -->
    <link href="css/login.css" rel="stylesheet" type="text/css"/>
    <!--<link href="assets/css/custom.css" rel="stylesheet" type="text/css"/> -->
    <!-- END STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>

    <script>
        $(function() {
            $( "#tabs" ).tabs();
        });
        function mypasswordmatch()
        {
            var pass1 = $("#password").val();
            var pass2 = $("#password2").val();
            if (pass1 != pass2)
            {
                alert("Passwords do not match");
                var nomatch = "Passwords do not match";
                return false;
            }
            else
            {
                $( "#reset" ).submit();
            }
        }
    </script>

</head>
<!--  END HEAD -->


<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.php">
        <img src="assets/img/logo-big.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->


<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form fields" action="reset.php" method="post" id="reset">
        <h3 class="form-title">Reset your account's password</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>
    Enter New Password and Confirm New Password
                
    </span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label for="password" class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Enter New Password" name="password" id="password"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password2" class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm New Password" name="password2" id="password2"/>
                <input name="action" type="hidden" value="<?php echo $encrypt ?>" />
            </div>
        </div>
        <div class="form-actions">

            <button type="submit" value="Reset password" onclick="mypasswordmatch();" class="btn blue pull-right">
    Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
            <input type="hidden" name="token" value="<?php echo CHRM_Token::generate(); ?>" >
        </div>


    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="" method="post">
        <h3>Forget Password ?</h3>
        <p>
    Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
                <input name="action" type="hidden" value="password" />
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i> Back </button>
            <button type="submit" class="btn blue pull-right">
    Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>

        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
    2014 &copy; CoffeeHRM - Osama Iqbal.
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <![endif]-->
    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/scripts/core/app.js" type="text/javascript"></script>
    <script src="assets/scripts/custom/login-soft.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
    jQuery(document).ready(function() {
        App.init();
        Login.init();
    });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>