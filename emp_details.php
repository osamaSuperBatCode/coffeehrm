<?php
$j = 0;

require_once 'inc/core/init.php';


$user = new CHRM_User();
if($user->isLoggedIn())
{
    if($user->hasPermission('admin')) {
        CHRM_Redirect::to('dashboard.php');
    }
    if(!$user->hasPermission('admin'))
    {

    }
}

if (empty($_SESSION['user'])) {
    header('Location: index.php');
    echo 'This Worked!';
    exit;
}

$sessionName = CHRM_Config::get('session/session_name');
if(!CHRM_Session::exists($sessionName)) {
    CHRM_Redirect::to('index.php');
}


////////////////////////////////////////INSERT VALUES BEGINS HERE ///////////////////////////////////////////////////

if(isset($_POST['add']))
{
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conn = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerify = 'display-hide'; //for the popup to show that update has been sucessful

    if(get_magic_quotes_gpc()){
        $emp_id = $_POST['id'];
        $emp_no = $_POST['empno'];
        $emp_fname = $_POST['firstname'];
        $emp_mname = $_POST['middlename'];
        $emp_lname = $_POST['lastname'];
        $emp_dob = $_POST['dob'];
        $emp_gender = $_POST['gender'];
        $emp_maritalstatus = $_POST['maritalstatus'];
        $emp_adharcardno = $_POST['adharcardno'];
        $emp_otherid = $_POST['otherid'];
        $emp_driverlicense = $_POST['driverlicense'];
        $emp_workstationid = $_POST['workstationid'];
        $emp_address1 = $_POST['address1'];
        $emp_address2 = $_POST['address2'];
        $emp_country = $_POST['country'];
        $emp_city = $_POST['city'];
        $emp_zipcode = $_POST['zipcode'];
        $emp_homephone = $_POST['homephone'];
        $emp_mobilephone = $_POST['mobilephone'];
        $emp_workphone = $_POST['workphone'];
        $emp_workemail = $_POST['workemail'];
        $emp_privateemail = $_POST['privateemail'];
        $emp_joineddate = $_POST['joineddate'];
        $emp_confirmdate = $_POST['confirmdate'];
    } else {
        $emp_id = $_POST['id'];
        $emp_no = $_POST['empno'];
        $emp_fname = $_POST['firstname'];
        $emp_mname = $_POST['middlename'];
        $emp_lname = $_POST['lastname'];
        $emp_dob = $_POST['dob'];
        $emp_gender = $_POST['gender'];
        $emp_maritalstatus = $_POST['maritalstatus'];
        $emp_adharcardno = $_POST['adharcardno'];
        $emp_otherid = $_POST['otherid'];
        $emp_driverlicense = $_POST['driverlicense'];
        $emp_workstationid = $_POST['workstationid'];
        $emp_address1 = $_POST['address1'];
        $emp_address2 = $_POST['address2'];
        $emp_country = $_POST['country'];
        $emp_city = $_POST['city'];
        $emp_zipcode = $_POST['zipcode'];
        $emp_homephone = $_POST['homephone'];
        $emp_mobilephone = $_POST['mobilephone'];
        $emp_workphone = $_POST['workphone'];
        $emp_workemail = $_POST['workemail'];
        $emp_privateemail = $_POST['privateemail'];
        $emp_joineddate = $_POST['joineddate'];
        $emp_confirmdate = $_POST['confirmdate'];
    }


    $sql = "INSERT INTO employees ".
        "(id,empno ,firstname,middlename,lastname,dob,gender,maritalstatus,adharcardno,otherid,driverlicense,workstationid ,address1 ,address2 ,country ,city ,zipcode ,homephone ,mobilephone ,workphone ,workemail ,privateemail,joineddate ,confirmdate)".
        "VALUES('$emp_id','$emp_no' ,'$emp_fname','$emp_mname','$emp_lname','$emp_dob','$emp_gender','$emp_maritalstatus','$emp_adharcardno','$emp_otherid','$emp_driverlicense','$emp_workstationid','$emp_address1','$emp_address2' ,'$emp_country','$emp_city','$emp_zipcode' ,'$emp_homephone' ,'$emp_mobilephone' ,'$emp_workphone' ,'$emp_workemail' ,'$emp_privateemail','$emp_joineddate' ,'$emp_confirmdate')" ;

    mysql_select_db('coffeehrm');

    $retval = mysql_query( $sql, $conn );
    if(! $retval )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conn);

}


//////////////////////////////////////INSERT VALUES ENDS HERE ////////////////////////////////////////////////////////


/////////////////////////////////FIRST PORTLET UPDATE START //////////////////////////////////////////////////////////
if(isset($_POST['update']))
{
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conn = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conn )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerify = 'display-hide'; //for the popup to show that update has been sucessful

    $emp_id = $_POST['id'];
    $emp_no = $_POST['empno'];
    $emp_fname = $_POST['firstname'];
    $emp_mname = $_POST['middlename'];
    $emp_lname = $_POST['lastname'];
    $emp_dob = $_POST['dob'];
    $emp_gender = $_POST['gender'];
    $emp_maritalstatus = $_POST['maritalstatus'];
    $emp_adharcardno = $_POST['adharcardno'];
    $emp_otherid = $_POST['otherid'];
    $emp_driverlicense = $_POST['driverlicense'];
    $emp_workstationid = $_POST['workstationid'];
    $emp_address1 = $_POST['address1'];
    $emp_address2 = $_POST['address2'];
    $emp_country = $_POST['country'];
    $emp_city = $_POST['city'];
    $emp_zipcode = $_POST['zipcode'];
    $emp_homephone = $_POST['homephone'];
    $emp_mobilephone = $_POST['mobilephone'];
    $emp_workphone = $_POST['workphone'];
    $emp_workemail = $_POST['workemail'];
    $emp_privateemail = $_POST['privateemail'];
    $emp_joineddate = $_POST['joineddate'];
    $emp_confirmdate = $_POST['confirmdate'];

    $sql = "UPDATE employees ".
        "SET empno = '$emp_no', firstname = '$emp_fname', middlename = '$emp_mname', lastname = '$emp_lname', dob = '$emp_dob', gender = '$emp_gender', maritalstatus = '$emp_maritalstatus',
             adharcardno = '$emp_adharcardno', otherid = '$emp_otherid', driverlicense = '$emp_driverlicense', workstationid = '$emp_workstationid',
              address1 = '$emp_address1', address2 = '$emp_address2', country = '$emp_country', city = '$emp_city', zipcode = '$emp_zipcode',
               homephone = '$emp_homephone', mobilephone = '$emp_mobilephone',workphone = '$emp_workphone',workemail = '$emp_workemail',
               privateemail = '$emp_privateemail', joineddate = '$emp_joineddate',confirmdate = '$emp_confirmdate'".
        "WHERE id = $emp_id" ;

    mysql_select_db('coffeehrm');

    $retval = mysql_query( $sql, $conn );
    if(! $retval )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conn);

}
///////////////////////////////////////////////////FIRST PORTLET UPDATE END /////////////////////////////////////////////

//////////////////////////////////////////////////////FIRST PORTLET DELETE START/////////////////////////////////////////
if(isset($_POST['delete'])) {
    $db_host = 'localhost';
    $db_user = 'root';
    $db_pw = '';

    $conndel1 = mysql_connect($db_host, $db_user, $db_pw);
    if(! $conndel1 )
    {
        die('Could not connect: ' . mysql_error());
    }

    $displayVerifydel = 'display-hide'; //for the popup to show that update has been sucessful

    $emp_id = $_POST['id'];
    $emp_no = $_POST['empno'];
    $emp_fname = $_POST['firstname'];
    $emp_mname = $_POST['middlename'];
    $emp_lname = $_POST['lastname'];

    $sqldel = "DELETE FROM employees ".
        "WHERE id = $emp_id" ;

    mysql_select_db('coffeehrm');

    $retvaldel = mysql_query( $sqldel, $conndel1 );
    if(! $retvaldel )
    {
        die('Could not update data: ' . mysql_error());
    }
    else{
        $displayVerify = '';
    }

    mysql_close($conndel1);

}
////////////////////////////////////////////////////FIRST PORTLET DELETE END /////////////////////////////////////////////



?>

<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>CoffeeHRM | Employee Details</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="Osama Iqbal" name="author"/>




    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->






    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <![endif]-->


    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/tableExport.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/scripts/core/app.js"></script>
    <script src="assets/scripts/custom/table-editable.js"></script>



    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>


    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="css/style-coffeehrm.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">

<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="dashboard.php">
            <img src="assets/img/logo.png" alt="logo" class="img-responsive"/>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">


            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username">
						 Welcome
					</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="profile.php">
                            <i class="fa fa-user"></i> My Profile
                        </a>
                    </li>
                    <li class="divider">
                    </li>
                    <li>
                        <a href="javascript:;" id="trigger_fullscreen">
                            <i class="fa fa-arrows"></i> Full Screen
                        </a>
                    </li>
                    <li>
                        <a href="logout.php">
                            <i class="fa fa-key"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>

            <li class = "start">
                <a href="emp_details.php">
                    <i class="fa fa-book"></i>
						<span class="title">
							Deatils
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
            <li>
                <a href="emp_leaves.php">
                    <i class="fa fa-calendar"></i>
						<span class="title">
							Leave Application
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li class ="last">
                <a href="emp_attendance.php">
                    <i class="fa fa-clock-o"></i>
						<span class="title">
							Register Attendance
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR --><!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Employee Details <small>Update Employee Details</small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">

            <li>
                <i class="fa fa-home"></i>
                <a href="dashboard.php">
                    Home
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">
                    Admin
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="employees.php">
                    Employee Details
                </a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">

<div class="portlet box blue">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Basic Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_1').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_1').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
<tr>
    <th>Id</th>
    <th>
        Employee Number
    </th>
    <th>
        First Name
    </th>
    <th>
        Middle Name
    </th>
    <th>
        Last Name
    </th>

</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
    if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td><?php echo $row['id'];?></td>
    <td><?php echo $row['empno']?></td>
    <td><?php echo $row['firstname']?></td>
    <td><?php echo $row['middlename']?></td>
    <td><?php echo $row['lastname']?></td>

    </tr>
    <?php $i++;$j++; } } ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<script>
    $(document).ready(function(){

        $('a.edit').click(function(){
            var id = $(this).data('id');
            var empno = $(this).data('empno');
            var firstname = $(this).data('firstname');
            var middlename = $(this).data('middlename');
            var lastname = $(this).data('lastname');

            $('#myid').val(id);
            $('#myempno').val(empno);
            $('#myfirstname').val(firstname);
            $('#mymiddlename').val(middlename)
            $('#mylastname').val(lastname);

        });

    });
</script>

<!-- YOUR MODAL GOES HERE  -->
<div class="modal fade" id="portlet-configadd"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-wide modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Add New Employee</h4>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<!-- BEGIN VALIDATION STATES-->

<div class="portlet-body form">
<!-- BEGIN FORM-->
<form id="form_sample_1" class="form-horizontal empadd" name="empadd" action="employees.php" method="post">
<div class="form-body">
<div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button>
    Values have not been updated. You have some form errors. Please check below.
</div>
<div class="alert alert-success display-hide">
    <button class="close" data-close="alert"></button>
    Values have been added sucessfully.
</div>
<div class="form-group">
    <label class="control-label col-md-3">Employee Number
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">

        <input required type="text" name="empno" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">First Name
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" id="abc" name="firstname" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Middle Name

    </label>
    <div class="col-md-4">
        <input type="text" id="abc" name="middlename" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Last Name
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" name="lastname" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Date Of Birth
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <div required class="input-group date date-picker input-large" data-date-format="yyyy-mm-dd">

            <!--<span class="input-group-btn">
                <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
            </span>
<span class="input-group-btn">
                <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
            </span> -->
            <input name="dob" required type="date" class="form-control" >

        </div>


    </div>

</div>


<div class="form-group">
    <label class="control-label col-md-3">Gender
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <select required name="gender" data-required="1"  class="form-control input-large">
            <option value="">Select One</option>
            <option value = "Male">Male</option>
            <option value = "Female">Female</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Marital Status
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <select required name="maritalstatus" data-required="1"  class="form-control input-large">
            <option value="">Select One</option>
            <option value = "Single">Single</option>
            <option value = "Married">Married</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Adhar Card Number

    </label>
    <div class="col-md-4">
        <input type="text" name="adharcardno" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Other Id

    </label>
    <div class="col-md-4">
        <input type="text" name="otherid" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Driver License

    </label>
    <div class="col-md-4">
        <input type="text" name="driverlicense" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Workstation Id

    </label>
    <div class="col-md-4">
        <input type="text" name="workstationid" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Address 1
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" name="address1" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Address 2
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" name="address2" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Country
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" name="country" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">City
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="text" name="city" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Zip Code
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="number" name="zipcode" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Home Phone

    </label>
    <div class="col-md-4">
        <input type="number"  name="homephone" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Mobile Phone

    </label>
    <div class="col-md-4">
        <input required type="number" name="mobilephone" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Work Phone

    </label>
    <div class="col-md-4">
        <input type="number"  name="workphone" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Work Email
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="email" name="workemail" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Private Email
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <input required type="email" name="privateemail" data-required="1" autocomplete="off" class="form-control input-large"/>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Joined Date
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <div required class="input-group date date-picker input-large" data-date-format="yyyy-mm-dd">

            <!--<span class="input-group-btn">
                <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
            </span>
<span class="input-group-btn">
                <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
            </span>-->
            <input  name="joineddate" required type="date" class="form-control">

        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3">Confirmation Date
										<span class="required">
											 *
										</span>
    </label>
    <div class="col-md-4">
        <div required class="input-group date date-picker input-large" data-date-format="yyyy-mm-dd">

            <!--<span class="input-group-btn">
                <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
            </span>
<span class="input-group-btn">
                <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
            </span> -->
            <input  name="confirmdate" required type="date" class="form-control">

        </div>
    </div>
</div>



<div class="modal-footer form-actions fluid">
    <input id="add" name="add" type="submit" class="btn btn-success" value="Save Changes" onclick="setTimeout(ajaxCall(),2000)">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
</div>
</form>

<!-- END FORM-->
</div>

<!-- END VALIDATION STATES-->
</div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div></div>
<script>
    function ajaxCall(){
        $(document).ready(function () {
            $("input#add").click(function(e){

                $.ajax({
                    type: "POST",
                    url: "employees.php", //
                    data: $('form.empadd').serialize(), //class of the form
                    success: function(msg){
                        //$("#thanks").html(msg);

                        //alert($('#abc').val());
                        $("#portlet-configadd").modal('hide');	// modal id
                    },
                    error: function(){
                        alert("failure");
                    }
                });
            });
        });
    }
</script>
</div>



<!-------------------------------------------------- SECOND PORTLET -------------------------------------------------------->

<div class="col-md-12">

<div class="portlet box purple">
<div class="portlet-title">
    <div class="caption">Personal Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_2').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_2').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_2">
<thead>
<tr>
    <th></th>
    <th>Name</th>
    <th>
        Date Of Birth
    </th>
    <th>
        Gender
    </th>
    <th>
        Marital Status
    </th>
    <th>
        Adhar Card Number
    </th>

</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['firstname']?></td>

    <td><?php echo $row['dob']?></td>
    <td><?php echo $row['gender']?></td>
    <td><?php echo $row['maritalstatus']?></td>
    <td><?php echo $row['adharcardno']?></td>

    </tr>
    <?php $i++;$j++; }} ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>


<!--------------------------------------------------- THIRD PORTLET ----------------------------------------------------------->
<div class="col-md-12">

<div class="portlet box yellow">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Employee ID Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

      <!--  <ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_3').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_3').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_3">
<thead>
<tr>
    <th></th>
    <th>Name</th>
    <th>
        Other Id
    </th>
    <th>
        Drivers License
    </th>
    <th>
        Workstation Id
    </th>
    <th>
        Address One
    </th>

</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['firstname']?></td>
    <td><?php echo $row['otherid']?></td>
    <td><?php echo $row['driverlicense']?></td>
    <td><?php echo $row['workstationid']?></td>
    <td><?php echo $row['address1']?></td>

    </tr>
    <?php $i++;$j++; }} ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>


<!---------------------------------------------------- FOURTH PORTLET -------------------------------------------------------------------->

<div class="col-md-12">

<div class="portlet box blue">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Residential Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_4').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_4').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_4">
<thead>
<tr>
    <th></th>
    <th>Name</th>
    <th>
        Address Two
    </th>
    <th>
        Country
    </th>
    <th>
        City
    </th>
    <th>
        Zip Code
    </th>

</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['firstname']?></td>
    <td><?php echo $row['address2']?></td>
    <td><?php echo $row['country']?></td>
    <td><?php echo $row['city']?></td>
    <td><?php echo $row['zipcode']?></td>

    </tr>
    <?php $i++;$j++; } }?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>

<!-----------------------------------------------------FIFTH PORTLET ----------------------------------------------------------->
<div class="col-md-12">

<div class="portlet box green">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Contact Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_5').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_5').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_5">
<thead>
<tr>
    <th></th>
    <th>Name</th>
    <th>
        Home Phone
    </th>
    <th>
        Mobile Phone
    </th>
    <th>
        Work Phone
    </th>
    <th>
        Work Email
    </th>

</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['firstname']?></td>
    <td><?php echo $row['homephone']?></td>
    <td><?php echo $row['mobilephone']?></td>
    <td><?php echo $row['workphone']?></td>
    <td><?php echo $row['workemail']?></td>

    </tr>
    <?php $i++;$j++; }} ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!----------------------------------------------------- SIXTH PROTLET ---------------------------------------------------------->
<div class="col-md-12">

<div class="portlet box grey">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Company Affiliation Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">


    <div class="btn-group pull-right">

        <!--<ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_6').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_6').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>-->
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_6">
<thead>
<tr>
    <th></th>
    <th>Name</th>
    <th>
        Private Email
    </th>
    <th>
        Joined Date
    </th>
    <th>
        Confirmation Date
    </th>


</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
if($user->data()->empid == $row['id']) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['firstname']; ?></td>
    <td><?php echo $row['privateemail']?></td>
    <td><?php echo $row['joineddate']?></td>
    <td><?php echo $row['confirmdate']?></td>

    </tr>
    <?php $i++;$j++; }} ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
<!-- END PAGE CONTENT -->
</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2014 &copy; CoffeeHRM by Osama Iqbal and Chirag Parasrampuria.

    </div>
    <div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
    </div>
</div>
<!-- END FOOTER -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->


<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>

<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        TableEditable.init();


    });
</script>


<script src="assets/scripts/custom/table-editable.js"></script>



<!--<script>
    $(function(){
        $('#myFormSubmit').click(function(e){
            e.preventDefault();
            alert($('#abc').val());
            /*
             $.post('http://path/to/post',
             $('#myForm').serialize(),
             function(data, status, xhr){
             // do something here with response;
             });
             */
        });
    });
</script> -->

<!-- BEGIN PAGE LEVEL STYLES -->






<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->





<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/tableExport.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/table-editable.js"></script>






<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/form-validation.js"></script>
<!-- END PAGE LEVEL STYLES -->


</body>
<!-- END BODY -->
</html>


