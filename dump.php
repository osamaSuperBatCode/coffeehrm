<div class="col-md-12">

<div class="portlet box green">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Employee Details
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">
    <div class="btn-group">
        <a href="#portlet-configadd" data-toggle="modal" class="config">
            <button class="btn yellow">
                Add New <i class="fa fa-plus"></i>
            </button>
        </a>
    </div>

    <div class="btn-group pull-right">

        <ul class="pull-right" style=" list-style-type: none; ">
            <li>
                <button class="btn red">
                    Save as PDF <i class="fa fa-copy"></i>
                    <a href="#" onClick ="$('#sample_editable_6').tableExport({type:'pdf',escape:'false', ignoreColumn:'[0,5,6]', pdfFontSize:10, pdfLeftMargin:-35, tableName:'yourTableName'});" target="_blank">
                    </a></button>
                <button class="btn green">
                    Export to Excel <i class="fa fa-th"></i>
                    <a href="#" onClick ="$('#sample_editable_6').tableExport({type:'excel',escape:'false', ignoreColumn:'[0,5,6]', htmlContent:'false', tableName:'yourTableName'});">
                    </a></button>
            </li>

        </ul>
    </div>
</div>
<div class="form-body">
    <div class="alert alert-success <?php if(isset($emp_id)) { echo '';} else {echo 'display-hide';} ?>">
        <button class="close" data-close="alert"></button>
        Values have been updated successfully.
    </div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_6">
<thead>
<tr>
    <th></th>
    <th>
        Private Email
    </th>
    <th>
        Joined Date
    </th>
    <th>
        Confirmation Date
    </th>

    <th>
        Edit
    </th>
    <th>
        Delete
    </th>
</tr>
</thead>
<tbody>
<?php

$server = mysql_connect('localhost', 'root', '');
$db =  mysql_select_db('CoffeeHRM',$server);
$query = mysql_query("select * from employees");
$i = 1;



while ($row = mysql_fetch_array($query)) {
    ?>

    <tr>
    <td></td>
    <td><?php echo $row['privateemail']?></td>
    <td><?php echo $row['joineddate']?></td>
    <td><?php echo $row['confirmdate']?></td>
    <td><div id="thanks"><a class = "" data-toggle="modal" href = "#portlet-config_6<?php echo $i; ?>" data-id = '"<?php echo $row['id']; ?>"' data-empno ='"<?php echo $row['empno']; ?>"' data-firstname = '"<?php echo $row['firstname']; ?>"' data-lastname ='"<?php echo $row['lastname']; ?>"'>
                Edit
            </a></div>
        <script>
            $(document).ready(function(){

                $('a.edit').click(function(){
                    var id = $(this).data('id');
                    var empno = $(this).data('empno');
                    var firstname = $(this).data('firstname');
                    var middlename = $(this).data('middlename');
                    var lastname = $(this).data('lastname');

                    $('#myid').val(id);
                    $('#myempno').val(empno);
                    $('#myfirstname').val(firstname);
                    $('#mymiddlename').val(middlename)
                    $('#mylastname').val(lastname);

                });

            });
        </script>

        <!-- YOUR MODAL GOES HERE  -->
        <div class="modal fade" id="portlet-config_6<?php echo $i;?>"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-wide modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN VALIDATION STATES-->

                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form id="form_sample_1" class="form-horizontal emp6" name="emp6" action="" method="post">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have not been updated.You have some form errors. Please check below.
                                            </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have been updated successfully.
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Private Email

                                                </label>
                                                <span class="required">
											 *
										</span>
                                                <div class="col-md-4">
                                                    <input type = "hidden" name = "id" value = "<?php echo $row['id']; ?>">
                                                    <input type = "hidden" name = "empno" value = "<?php echo $row['empno']; ?>">
                                                    <input type = "hidden" name = "firstname" value = "<?php echo $row['firstname']; ?>">
                                                    <input type = "hidden" name = "middlename" value = "<?php echo $row['middlename']; ?>">
                                                    <input type = "hidden" name = "lastname" value = "<?php echo $row['lastname']; ?>">
                                                    <input type = "hidden" name = "dob" value = "<?php echo $row['dob']; ?>">
                                                    <input type = "hidden" name = "gender" value = "<?php echo $row['gender']; ?>">
                                                    <input type = "hidden" name = "maritalstatus" value = "<?php echo $row['maritalstatus']; ?>">
                                                    <input type = "hidden" name = "adharcardno" value = "<?php echo $row['adharcardno']; ?>">
                                                    <input type = "hidden" name = "otherid" value = "<?php echo $row['otherid']; ?>">
                                                    <input type = "hidden" name = "driverlicense" value = "<?php echo $row['driverlicense']; ?>">
                                                    <input type = "hidden" name = "workstationid" value = "<?php echo $row['workstationid']; ?>">
                                                    <input type = "hidden" name = "address1" value = "<?php echo $row['address1']; ?>">
                                                    <input type = "hidden" name = "address2" value = "<?php echo $row['address2']; ?>">
                                                    <input type = "hidden" name = "country" value = "<?php echo $row['country']; ?>">
                                                    <input type = "hidden" name = "city" value = "<?php echo $row['city']; ?>">
                                                    <input type = "hidden" name = "homephone" value = "<?php echo $row['homephone']; ?>">
                                                    <input type = "hidden" name = "mobilephone" value = "<?php echo $row['mobilephone']; ?>">
                                                    <input type = "hidden" name = "workphone" value = "<?php echo $row['workphone']; ?>">
                                                    <input type = "hidden" name = "workemail" value = "<?php echo $row['workemail']; ?>">
                                                    <input type = "hidden" name = "privateemail" value = "<?php echo $row['privateemail']; ?>">
                                                    <input type = "hidden" name = "joineddate" value = "<?php echo $row['joineddate']; ?>">
                                                    <input type = "hidden" name = "confirmdate" value = "<?php echo $row['confirmdate']; ?>">

                                                    <input type="email" name="privateemail" data-required="1" autocomplete="off" class="form-control input-large" value="<?php echo $row['privateemail']; ?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Joined Date

                                                </label>
                                                <span class="required">
											 *
										</span>
                                                <div class="col-md-4">
                                                    <div required class="input-group date date-picker input-large" data-date-format="yyyy-mm-dd">

															<span class="input-group-btn">
																<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
															</span>
                                                <span class="input-group-btn">
																<button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
															</span>
                                                        <input name="dob" required type="text" class="form-control" readonly value="<?php echo $row['joineddate']; ?>">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Confirmation Date

                                                </label>
                                    <span class="required">
											 *
										</span>
                                                <div class="col-md-4">
                                                    <div required class="input-group date date-picker input-large" data-date-format="yyyy-mm-dd">

															<span class="input-group-btn">
																<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
															</span>
                                                <span class="input-group-btn">
																<button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
															</span>
                                                        <input name="dob" required type="text" class="form-control" readonly value="<?php echo $row['confirmdate']; ?>">

                                                    </div>
                                                </div>
                                            </div>



                                            <div class="modal-footer form-actions fluid">
                                                <input id="update" name="update" type="submit" class="btn btn-success" value="Save Changes" onclick="setTimeout(ajaxCall(),2000)">
                                                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                            </div>
                                    </form>

                                    <!-- END FORM-->
                                </div>

                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div></div>
        <script>
            function ajaxCall(){
                $(document).ready(function () {
                    $("input#update").click(function(e){

                        $.ajax({
                            type: "POST",
                            url: "employees.php", //
                            data: $('form.emp6').serialize(), //class of the form
                            success: function(msg){
                                //$("#thanks").html(msg);

                                //alert($('#abc').val());
                                $("#portlet-config_6<?php echo $i; ?>").modal('hide');	// modal id
                            },
                            error: function(){
                                alert("failure");
                            }
                        });
                    });
                });
            }
        </script>
    </td>
    <td><a class = "" data-toggle="modal" href = "#portlet-configdel_6<?php echo $i; ?>" data-id = '"<?php echo $row['id']; ?>"' data-empno ='"<?php echo $row['empno']; ?>"' data-firstname = '"<?php echo $row['firstname']; ?>"' data-lastname ='"<?php echo $row['lastname']; ?>"'>
            Delete
        </a>
        <script>
            $(document).ready(function(){

                $('a.edit').click(function(){
                    var id = $(this).data('id');
                    var empno = $(this).data('empno');
                    var firstname = $(this).data('firstname');
                    var lastname = $(this).data('lastname');

                    $('#myid').val(id);
                    $('#myempno').val(empno);
                    $('#myfirstname').val(firstname);
                    $('#mylastname').val(lastname);
                });

            });
        </script>
        <div class="modal fade" id="portlet-configdel_6<?php echo $i;?>"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-wide modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Details</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form id="form_sample_1" class="form-horizontal emp6del" name="emp6" action="" method="post">
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have not been updated.You have some form errors. Please check below.
                                            </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button>
                                                Values have been updated successfully.
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-danger">
                                                    <button class="close" data-close="alert"></button>
                                                    Are you sure you want to delete <?php echo $row['firstname'] ,'\'s '?> details?
                                                    <input type = "hidden" name = "id" value = "<?php echo $row['id']; ?>">
                                                    <input type = "hidden" name = "empno" value = "<?php echo $row['empno']; ?>">
                                                    <input type = "hidden" name = "firstname" value = "<?php echo $row['firstname']; ?>">
                                                    <input type = "hidden" name = "middlename" value = "<?php echo $row['middlename']; ?>">
                                                    <input type = "hidden" name = "lastname" value = "<?php echo $row['lastname']; ?>">
                                                    <input type = "hidden" name = "dob" value = "<?php echo $row['dob']; ?>">
                                                    <input type = "hidden" name = "gender" value = "<?php echo $row['gender']; ?>">
                                                    <input type = "hidden" name = "maritalstatus" value = "<?php echo $row['maritalstatus']; ?>">
                                                    <input type = "hidden" name = "adharcardno" value = "<?php echo $row['adharcardno']; ?>">
                                                    <input type = "hidden" name = "otherid" value = "<?php echo $row['otherid']; ?>">
                                                    <input type = "hidden" name = "driverlicense" value = "<?php echo $row['driverlicense']; ?>">
                                                    <input type = "hidden" name = "workstationid" value = "<?php echo $row['workstationid']; ?>">
                                                    <input type = "hidden" name = "address1" value = "<?php echo $row['address1']; ?>">
                                                    <input type = "hidden" name = "address2" value = "<?php echo $row['address2']; ?>">
                                                    <input type = "hidden" name = "country" value = "<?php echo $row['country']; ?>">
                                                    <input type = "hidden" name = "city" value = "<?php echo $row['city']; ?>">
                                                    <input type = "hidden" name = "homephone" value = "<?php echo $row['homephone']; ?>">
                                                    <input type = "hidden" name = "mobilephone" value = "<?php echo $row['mobilephone']; ?>">
                                                    <input type = "hidden" name = "workphone" value = "<?php echo $row['workphone']; ?>">
                                                    <input type = "hidden" name = "workemail" value = "<?php echo $row['workemail']; ?>">
                                                    <input type = "hidden" name = "privateemail" value = "<?php echo $row['privateemail']; ?>">
                                                    <input type = "hidden" name = "joineddate" value = "<?php echo $row['joineddate']; ?>">
                                                    <input type = "hidden" name = "confirmdate" value = "<?php echo $row['confirmdate']; ?>">

                                                </div>
                                                <div class="modal-footer form-actions fluid">
                                                    <input id="delete" name="delete" type="submit" class="btn btn-success" value="Delete" onclick="setTimeout(ajaxCall(),2000)">
                                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                                </div>
                                    </form>

                                    <!-- END FORM-->
                                </div>

                                <!-- END VALIDATION STATES-->
                            </div>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div></div>
        <script>
            function ajaxCall(){
                $(document).ready(function () {
                    $("input#delete").click(function(e){

                        $.ajax({
                            type: "POST",
                            url: "employees.php", //
                            data: $('form.emp6del').serialize(), //class of the form
                            success: function(msg){
                                //$("#thanks").html(msg);

                                //alert($('#abc').val());
                                $("#portlet-configdel_6<?php echo $i; ?>").modal('hide');	// modal id
                            },
                            error: function(){
                                alert("failure");
                            }
                        });
                    });
                });
            }
        </script>
    </tr>
    <?php $i++;$j++; } ?>

</tbody>
</table>
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>