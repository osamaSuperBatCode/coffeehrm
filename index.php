<?php

    require_once 'inc/core/init.php';
    include('inc/db_mysql.php');

if(CHRM_Input::get('email'))
{
    $email      = CHRM_Input::get('email');
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) // Validate email address
    {
        $message =  "Invalid email address please type a valid email!!";
    }
    else
    {
        $queryResult = CHRM_DB::getInstance()->get('users',array('email','=',$email));

        //$query = "SELECT id FROM users where email='".$email."'";
        //$result = mysqli_query($connection,$query);
        //$Results = mysqli_fetch_array($result);

        if($queryResult >= 1)
        {
            $memberID = $queryResult->first()->username;
            $encrypt = md5($memberID);
            $message = "Your password reset link sent to your e-mail address.";
            $to=$email;
            $subject="Forget Password";
            $from = 'pilafoo@gmail.com';
            $body='Hi, <br/> <br/>Your Membership ID is '.$memberID.' <br><br>Click here to reset your password http://127.0.0.1/coffeehrm/reset.php?encrypt='.$encrypt.'&action=reset   <br/> <br/>';
            $headers = "From: " . strip_tags($from) . "\r\n";
            $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            mail($to,$subject,$body,$headers);


            //$query = "SELECT id FROM users where md5(90*13+id)='".$encrypt."'";
//                $Results = mysqli_fetch_array($result);
//                print_r($Results);
//                $message = $encrypt. $query;
        }
        else
        {
            $message = "Account not found please signup now!!";
        }
    }
}



$sessionName = CHRM_Config::get('session/session_name');
    if(CHRM_Session::exists($sessionName)) {
        CHRM_Redirect::to('dashboard.php');
    }

    if(CHRM_Input::exists()) {
        if(CHRM_Token::check(CHRM_Input::get('token'))) {

            $validate = new CHRM_Validation();
            $validation = $validate->check($_POST, array(

                'username' => array('require' => true),
                'password' => array('required' => true)

            ));





            if($validation->passed()) {

                $user = new CHRM_User();
                $remember = (CHRM_Input::get('remember') === 'on') ? true : false;
                $login = $user->login(CHRM_Input::get('username'), CHRM_Input::get('password'), $remember);


                if($login) {
                    CHRM_Redirect::to('dashboard.php');
                    //echo 'OK!';
                } else {
                    //echo 'NOT OK!';
                }

            } else {
                foreach($validation->errors() as $error) {
                   echo '<div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
			                    <span>',
                                 $error . '<br>'
			                    ,'</span>
                         </div>';
                }



            }


        }

    }

?>

<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Coffee HRM | Enables More Coffee Breaks</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="Coffee HRM" name="description"/>
    <meta content="Osama Iqbal" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL STYLES -->
    <!--<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>-->
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- BEGIN STYLES -->
    <link href="css/style-coffeehrm.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <!--<link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/> -->
    <!--<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/> -->
    <!--<link href="assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/> -->
    <link href="css/login.css" rel="stylesheet" type="text/css"/>
    <!--<link href="assets/css/custom.css" rel="stylesheet" type="text/css"/> -->
    <!-- END STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>

</head>
<!--  END HEAD -->


<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.php">
        <img src="assets/img/logo-big.png" alt=""/>
    </a>
</div>
<!-- END LOGO -->


<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form fields" action="" method="POST">
        <h3 class="form-title">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
			<span>
				 Enter Username and Password
			</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label for="username" class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" id="password"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password"/>
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input type="checkbox" name="remember" id="remember"/> Remember me </label>
            <button type="submit" class="btn blue pull-right">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>
            <input type="hidden" name="token" value="<?php echo CHRM_Token::generate(); ?>" >
        </div>

        <!--<div class="forget-password">
            <h4> password ? click <a href="javascript:;" id="forget-password">
                    here
                </a></h4>

        </div>-->
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="" method="post">
        <h3>Forget Password ?</h3>
        <p>
            Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
                <input name="action" type="hidden" value="password" />
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i> Back </button>
            <button type="submit" class="btn blue pull-right">
                Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>

        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

    <!-- BEGIN COPYRIGHT -->
    <div class="copyright">
        2014 &copy; CoffeeHRM - Osama and Chirag.
    </div>
    <!-- END COPYRIGHT -->
    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <![endif]-->
    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/scripts/core/app.js" type="text/javascript"></script>
    <script src="assets/scripts/custom/login-soft.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {
            App.init();
            Login.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>