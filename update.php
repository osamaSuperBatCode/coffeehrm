<?php

    require_once 'inc/core/init.php';

    $user = new CHRM_User();

    if(!$user->isLoggedIn()) {
        CHRM_Redirect::to('testindex.php');
    }

    if(CHRM_Input::exists()) {
        if(CHRM_Token::check(CHRM_Input::get('token'))) {
            //echo 'OK!';

            $validate = new CHRM_Validation();
            $validation = $validate->check($_POST, array(
                'email' => array(
                    'require' => true,
                    'mix' => 2,
                    'max' => 50
                )
            ));

            if($validation->passed()) {

                try {
                    $user->update(array(
                        'email' =>CHRM_Input::get('email')
                    ));

                    CHRM_Session::flash('home', 'Your Details have been updated');
                    CHRM_Redirect::to('testindex.php');

                } catch(Exception $e) {
                    die($e->getMessage());
                }

            } else {
                foreach($validation->errors() as $error) {
                    echo $error, '<br>';
                }
            }
        }


    }
?>

<form action="" method="post">
    <div class="field">
        <label for="email">Email</label>
        <input type="text" name="email" value="<?php echo escape($user->data()->email); ?>" >

        <input type="submit" value="Update">
        <input type="hidden" name="token" value="<?php echo CHRM_Token::generate(); ?>">
    </div>
</form>