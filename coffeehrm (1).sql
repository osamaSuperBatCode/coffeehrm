-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2014 at 04:55 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `coffeehrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `atendance`
--

CREATE TABLE IF NOT EXISTS `atendance` (
  `id` int(50) NOT NULL,
  `timein` time NOT NULL,
  `timeout` time NOT NULL,
  `notice` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `details` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `url` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `details`, `address`, `email`, `url`) VALUES
(1, 'test', 'test', 'test', 'test@t.com', 'http://t.com'),
(2, '', '', 'Test', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `company_structure`
--

CREATE TABLE IF NOT EXISTS `company_structure` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `company_structure`
--

INSERT INTO `company_structure` (`id`, `name`, `country`, `address`, `type`) VALUES
(1, 'Mumbai Branch', 'India', 'Andheri', 'Head Branch'),
(3, 'Pune Branch', 'India', 'Near Airport', 'Sales Branch');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `dateadded` date NOT NULL,
  `details` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `dateadded`, `details`, `status`) VALUES
(1, '1993-02-21', 'Details', 'Active'),
(2, '0000-00-00', 'jkhdfg', 'jkdbfg'),
(3, '0000-00-00', 'hjg', 'hjg'),
(4, '0000-00-00', 'details', 'details'),
(5, '2014-10-09', 'Test', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empno` varchar(20) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(50) NOT NULL,
  `maritalstatus` varchar(50) NOT NULL,
  `adharcardno` varchar(50) NOT NULL,
  `otherid` varchar(50) NOT NULL,
  `driverlicense` varchar(50) NOT NULL,
  `workstationid` varchar(50) NOT NULL,
  `address1` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zipcode` int(50) NOT NULL,
  `homephone` int(50) NOT NULL,
  `mobilephone` int(50) NOT NULL,
  `workphone` int(50) NOT NULL,
  `workemail` varchar(50) NOT NULL,
  `privateemail` varchar(50) NOT NULL,
  `joineddate` date NOT NULL,
  `confirmdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `empno`, `firstname`, `middlename`, `lastname`, `dob`, `gender`, `maritalstatus`, `adharcardno`, `otherid`, `driverlicense`, `workstationid`, `address1`, `address2`, `country`, `city`, `zipcode`, `homephone`, `mobilephone`, `workphone`, `workemail`, `privateemail`, `joineddate`, `confirmdate`) VALUES
(1, '05', 'Alexx', 'M.', 'Garrett', '0000-00-00', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '0000-00-00', '0000-00-00'),
(2, 'EMP008', 'Billy', 'A.', 'Garrett', '0000-00-00', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '0000-00-00', '0000-00-00'),
(10, 'Testing', 'Testing', 'Test', 'Test', '2014-10-07', 'Male', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 0, 0, 0, 0, 'test', 'test', '0000-00-00', '0000-00-00'),
(14, 'g', 'jhbhjb', 'hjbjh', 'hjbjhb', '0000-00-00', 'Single', 'Male', 'hjbjhb', 'hjbj', 'hbjh', 'bjhb', 'hjb', 'jhbjh', 'bjh', 'bjhb', 0, 555, 555, 555, 'sdf@a.com', 'sjkdnb@a.com', '0000-00-00', '0000-00-00'),
(15, 'jkn', 'jkbnjknkj', 'nkjn', 'kjnk', '2014-10-07', 'Male', 'Male', 'dfgn', 'jnjk', 'nkj', 'nkj', 'nkj', 'nkjn', 'jk', 'nkj', 5651, 56165, 1651, 651, '651@a.com', 'sdf@b.com', '2014-10-07', '2014-10-23'),
(16, 'test', 'test', 'test', 'test', '2014-10-15', 'Male', 'Single', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 50, 50, 50, 50, 'a@a.com', 'b@b.com', '2014-10-16', '2014-10-24'),
(17, 'Chirag', 'rfwefwef', 'sedfwef', 'sdfsdfsf', '2014-10-16', 'Male', 'Single', 'kjhkuh', 'kjhkjh', 'kjhkjhkjh', 'kjhkjhkj', 'kjhkjh', 'kjhkj', 'kjhkjh', 'kjhkjh', 0, 6541654, 654654, 56465, 'jkbhj@asdf.com', 'ksdljf@ksldfng.com', '2014-10-10', '2014-10-10'),
(18, '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '0000-00-00', '0000-00-00'),
(19, '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '0000-00-00', '0000-00-00'),
(20, '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', 'test', '', 0, 0, 0, 0, '', '', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `employees_ghost`
--

CREATE TABLE IF NOT EXISTS `employees_ghost` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `employmentstatus` varchar(50) NOT NULL,
  `jobtitle` varchar(50) NOT NULL,
  `paygrade` varchar(100) NOT NULL,
  `department` varchar(50) NOT NULL,
  `supervisor` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `permission` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `username`, `permission`) VALUES
(1, 'Employee', ''),
(2, 'Administrator', '{"admin": 1}');

-- --------------------------------------------------------

--
-- Table structure for table `job_titles`
--

CREATE TABLE IF NOT EXISTS `job_titles` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `jobtitlecode` varchar(50) NOT NULL,
  `jobtitle` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `specefication` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `job_titles`
--

INSERT INTO `job_titles` (`id`, `jobtitlecode`, `jobtitle`, `description`, `specefication`) VALUES
(1, 'SD101', 'Software Developer', 'ASIC Software Development', 'ASIC, C++ Knowledge');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE IF NOT EXISTS `leaves` (
  `pkid` int(50) NOT NULL AUTO_INCREMENT,
  `id` int(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `fromdate` varchar(50) NOT NULL,
  `numberofdays` int(50) NOT NULL,
  `approve` varchar(50) NOT NULL,
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `leaves`
--

INSERT INTO `leaves` (`pkid`, `id`, `type`, `fromdate`, `numberofdays`, `approve`) VALUES
(1, 14, 'kjn', '2014-04-14', 2, 'Awaiting!'),
(3, 14, 'WAIT', '2014-04-14', 2, ''),
(4, 14, 'WAIT', '2014-04-14', 2, ''),
(7, 15, 'hjb', 'jhb', 2, 'hjb'),
(8, 15, 'hjb', 'jhb', 2, 'hjb');

-- --------------------------------------------------------

--
-- Table structure for table `monitor_attendance`
--

CREATE TABLE IF NOT EXISTS `monitor_attendance` (
  `pkid` int(50) NOT NULL AUTO_INCREMENT,
  `id` int(50) NOT NULL,
  `timein` datetime NOT NULL,
  `timeout` datetime NOT NULL,
  `note` varchar(200) NOT NULL,
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `monitor_attendance`
--

INSERT INTO `monitor_attendance` (`pkid`, `id`, `timein`, `timeout`, `note`) VALUES
(2, 15, '2014-10-09 05:20:00', '2014-10-09 05:25:00', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `pay_grades`
--

CREATE TABLE IF NOT EXISTS `pay_grades` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `nameofpaygrade` varchar(50) NOT NULL,
  `currency` int(50) NOT NULL,
  `minsal` int(50) NOT NULL,
  `maxsal` int(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pay_grades`
--

INSERT INTO `pay_grades` (`id`, `nameofpaygrade`, `currency`, `minsal`, `maxsal`) VALUES
(1, 't', 0, 0, 0),
(2, 'kjnhkj', 1, 25451, 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `details` varchar(200) NOT NULL,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `details`, `status`) VALUES
(1, 'skjdf', 'klnjdfg', 'kldnjfg'),
(2, 'skjdf', 'klnjdfg', 'kldnjfgklj');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL DEFAULT '0',
  `education` varchar(50) NOT NULL,
  `certification` varchar(50) NOT NULL,
  `languages` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `education`, `certification`, `languages`) VALUES
(1, 'te', 'test', 'te'),
(2, 'test', 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `empid` varchar(50) NOT NULL,
  `group` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `empid`, `group`) VALUES
(1, 'test', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', 'à¾À€Î»2×ÇùP)—¦?†ð™o¾\ZËìW›ãÕ<£½', 'test', 1),
(2, 'testusername', '9f735e0df9a1ddc702bf0a1a7b83033f9f7153a00c29de82cedadc9957289b05', '×‰|rÄyûÅo½ìÓQDÓôr¢".•O2ƒ×á+ÇAÝ', 'test@t.com', 1),
(3, 'regreg', '007a74a6db2449880702eb2c1600a182616bae1c1f391bbc4049441f2a29b0c2', 'µabté5!ói"%áÊÅ»Èq,ýë-³E^Ü', 'reg@a.com', 1),
(4, 'regereg', '007a74a6db2449880702eb2c1600a182616bae1c1f391bbc4049441f2a29b0c2', 'd™0[_WÝP-T2P{¿©ÇßE;hª;à†×™˜', 'r@r.com', 1),
(5, 'osama', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', '''Þ–‡V…ßèÈn‰(=I°Ä;_¼‚:†ûaåðÌ', 'o@o.com', 1),
(6, 'oop', '690ed3ee68986dc00db60c2cc240530186b385a54bf98274d58c72022cdae1e8', '@…nÀõãÈ•7MiÉ(Åóky»Œ<	¬gÔÃÂàÎ', 'oop', 1),
(7, 'alex', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'ÆÃæ*9±„k€ÙÄ(ã:JûJ*­˜ÝÉ¤¬ËKýÕã', 'pp', 1),
(8, 'aa', '961b6dd3ede3cb8ecbaacbd68de040cd78eb2ed5889130cceb4c49268ea4d506', 'æH^ÁF4Œ3#ZiÉšµ¶ØÀ`Ü‹“Îp¥\r"ê–ñ', 'aa', 1),
(9, 'aoao', '524c1b06c969f7c216d2627c61a03a7bfe1b08bc8198d16c95a5c19f30baf432', 'ä3ÐÄ>FbEÈç1®ñü›Î­:P´å|sz™—', 'aoao', 1),
(10, 'tete', '043d6c5097ea53ca880fbbfdb22451e80b07dda8901591a6574c99907a2e278e', 'ÌOô1Êñ“~0içÞ…ÈLŠA‘|”¤•qüzçÚ', 'tete', 1),
(11, 'tqtq', '60370fb292b6a1b9e3c75a346ec6ea356d504244b163d5bcf111c85b3bdedd57', '‹8ÿÉ!ÞÅ+òY!IÞ­R?¦z+“2+¾Þ|3§¼œ', 'tqtq', 1),
(12, 'twtw', 'e2e933890e6ed82223bf7c92495b9df1782fe964775226ef0a0a7cd733b6d344', 'ði¥ZbÿRÒ$…;Ð\nÊ*O›Ç¯éGÐÝ\0a`=v,', 'twtw', 1),
(13, 'tyty', 'a444ba3697e28b67e10aaf6c6450adc551314e43b31080e92f7660cb27c06e67', 'Ð&¤,•ð(ÿš"ðDæ·ËêØTKðÏr\r}Q.ÌÅ•#`', 'tyty', 1),
(14, 'osa', '96e0e500a07ebf3a8580f62f051698215dad66cba2469ec155f0436fcd01bcbd', 'j\nfn¹''Fµˆ¹ÄÂ&P½0©û	ÖÍ•* ', 'pilafoo@gmail.com', 2),
(15, 'alex', '2909433c5cbcb572fcb1517a0ae091eca9248723838e15fabf3729e1fe882238', '>oÈäx’vF¼G‘‡g|ýÊpè{ûëFÓ$î„A', 'alex', 1),
(16, 'meh', '8e996b18fff7b1669500b3ae9b0853162324ef5103338e0a84805877ef27e607', '‹æ2ÝxŒë½ÐUJG®W^¨¼+éPƒ><ŒêsŠŽ', 'mehh', 1),
(17, 'testing12', 'e3a976d06ddeeb3900cdfc249fb66a69773ad4325abb508215c8b4c00e8603c5', 'õC_.ÄAM½Qì ƒ2]G1Õ/"lªZuô*µ', 'pilafoo@gmail.com', 2),
(18, '10', 'a9bef4688b989c33dcf9330da50b8a67c34fdd2f69be593dd0dd8d342ca28e14', 'Å¹''ê¯MäÀŒ¡YŸ5Ñi4Ä"‘†»œŠ?º³²˜', 'test10@test.com', 2),
(19, '17', 'd02803ffdbee44642aee7a249657f11def1d72cf105ed3eaefbe3c703035312d', '7…=+aˆZTˆ;Üœa±«õ›väÄ üÅ˜c¤œÄ', 'test17@test.com', 1),
(20, '20', 'bcc64f12353416e03603ee602789dc054129e113a07b6895cb608e1a894102eb', 'q| æcD¢§€Z,Ç— \0RÙT1±ÇÙ%3	T÷', 't20@t.com', 1),
(21, '19', 'a45761e7956d15df580193f69e197c24319f55eed466ed2554df6e5c38b8dffe', 'Ô1…¶èÁë.·ê:%6þ9•I²k§ˆÈ–ê“úa»Á•', 'test@test.com', 2),
(22, 'testempl', 'd39d98c7627f5e10e2ddebb77c25c72f5e3735658533394fd404690570ac9b78', 'úÓC$­e¬ø¾b?+ÁyÃ€#ÎôÕI.– F‘', 'abc@def.com', 1),
(23, '16', '47faf22598bed0a38c08269443902a0995f4c9694e5c5b99da22cb8a70e5f5ba', '+»6d9CüO¨È·Ýáp/ì¡?gËz©m£K', 'test16@test.com', 1),
(24, '15', '690daf5557f94933c24e45ec0792c9a46602264b90a5f4d53406c78a9fe09655', 'ýÃ(êÓ)’,~Á¨Ò¶n\\ZÎ!i7ÿê5]>MÅ', 'test15', 1),
(25, '22', '5b55f12263ceb306f082f9f5618fbd877fcad8bd27ab782a8d1acb18d4212e0c', 'áâUýª~Sù›¿ZlÒæŸ¦Á:8Úï’¬"¦Ïœ±à', 'pw', 1),
(26, '22p', '969f7ab20eecd3f84b1ecff93b0e78e10cc09dfcb30095731180c9cce60598a1', 'Õ<§¦q[³p**îyŸóÉ2ú‡øO[ñ''\n(\r+¬', '22p', 1),
(27, 'testemp', 'bb9f023277d7cda87deea7f7e51a7ab1a59add23f330ca005f1ed0cc2ff3cbc1', 'çÛµ#t}d5â\Z”ÕÎãKH3 ×ÀòðãV2ªÙœ¡%', '14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_session`
--

CREATE TABLE IF NOT EXISTS `users_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users_session`
--

INSERT INTO `users_session` (`id`, `user_id`, `hash`) VALUES
(1, 14, 'b50fd50aa8bd4b996ef9a778da01752e91d95d444d293dcadbea6232666f75e7'),
(2, 16, 'b0c5e5ae0ab28b10fa14939b8f2d4eac193b5e01070a8484171d92332828f7e6');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `skills`
--
ALTER TABLE `skills`
  ADD CONSTRAINT `skills_ibfk_1` FOREIGN KEY (`id`) REFERENCES `employees` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
