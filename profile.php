<?php
    require_once 'inc/core/init.php';

    if(!$username = CHRM_Input::get('user')) {
        CHRM_Redirect::to('testindex.php');
    } else {
        //echo $username;
        $user = new CHRM_User($username);

        if(!$user->exists()) {
            CHRM_Redirect::to(404);
        } else {
            //echo 'exists';
            $data = $user->data();
        }

?>

    <h3><?php echo escape($data->username); ?></h3>
    <p>email: <?php echo escape($data->email); ?></p>

<?php

    }