<?php

    require_once 'inc/core/init.php';

    if(CHRM_Input::exists()) {



        if(CHRM_Token::check(CHRM_Input::get('token'))) {
           // CHRM_Input::get('username');
            $validate = new CHRM_Validation();
            $validation = $validate->check($_POST, array(
                'username' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 20,
                    //'unique' => 'users'
                ),
                'password' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 20
                ),
                'password_again' => array(
                    'required' => true,
                    'matches' => 'password'
                ),
                'email' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 50
                )
            ));

            if($validation->passed()) {
                $user = new CHRM_User();
                $salt = CHRM_Hash::salt(32);


                try {
                    $user->create(array(
                        'username' => CHRM_Input::get('username'),
                        'password' => CHRM_Hash::make(CHRM_Input::get('password'), $salt),
                        'salt' => $salt,
                        'email' => CHRM_Input::get('email'),
                        'group' => 1
                    ));

                    CHRM_Session::flash('home', 'You have been registered');
                    //CHRM_Redirect::to('testindex.php');
                } catch (Exception $e) {
                    die($e->getMessage());
                }
            } else {
                foreach($validation->errors() as $error) {
                    echo $error, '<br>';
                }
            }
        }
    }
?>

<form action="" method="post">

    <div class = "field">
        <label for="username">Username</label>
        <input type="text" id="username" name="username" value="<?php escape(CHRM_Input::get('username')); ?>" autocomplete="off">
    </div>

    <div class = "field">
        <label for="password">Password</label>
        <input type="password" id="password" name="password">
    </div>

    <div class = "field">
        <label for="password_again">Enter Password Again</label>
        <input type="password" id="password_again" name="password_again" value="">
    </div>

    <div class = "field">
        <label for="email">Enter Your Email</label>
        <input type="text" id="email" name="email" <?php escape(CHRM_Input::get('email')); ?> autocomplete="off">
    </div>

    <input type = "hidden" name = "token" value = "<?php echo CHRM_Token::generate()?>">
    <input type="submit" value="Register">

</form>