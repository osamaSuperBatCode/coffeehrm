<?php

    require_once 'inc/core/init.php';

    $user = new CHRM_User();

    if(!$user->isLoggedIn()) {
        CHRM_Redirect::to('testindex.php');
    }

    if(CHRM_Input::exists()) {
        if(CHRM_Token::check(CHRM_Input::get('token'))) {

            $validate = new CHRM_Validation();
            $validation = $validate->check($_POST, array(
                'password_current' => array(
                    'require' => true,
                    'mix' => 6
                ),
                'password_new' => array(
                    'require' => true,
                    'mix' => 6
                ),

                'password_new_again' => array(
                    'require' => true,
                    'mix' => 6,
                    'matches' => 'password_new'
                )
            ));

            if($validation->passed()) {

                if(CHRM_Hash::make(CHRM_Input::get('password_current'), $user->data()->salt) !== $user->data()->password) {
                    echo 'Your current password is wrong';
                } else {
                    $salt = CHRM_Hash::salt(32);
                    $user->update(array(
                       'password' => CHRM_Hash::make(CHRM_Input::get('password_new'), $salt),
                        'salt' => $salt
                    ));

                    CHRM_Session::flash('home', 'Your Password has been changed');
                    CHRM_Redirect::to('testindex.php');
                }

            } else {
                foreach($validation->errors() as $error) {
                    echo '$error', '<br>';
                }
            }

        }
    }

?>

<form action="" method="post">
    <div class = "field">
        <label for="password_current">Current Password</label>
        <input type="password" name="password_current" id="password_current">
    </div>

    <div class = "field">
        <label for="password_new">New Password</label>
        <input type="password" name="password_new" id="password_new">
    </div>

    <div class = "field">
        <label for="password_new_again">Repeat New Password</label>
        <input type="password" name="password_new_again" id="password_new_again">
    </div>

    <input type = "submit" value="Change">
    <input type = "hidden" name="token" value="<?php echo CHRM_Token::generate(); ?>">
</form>