<?php

require_once 'inc/core/init.php';

$user = new CHRM_User();
if($user->isLoggedIn())
{
    if($user->hasPermission('admin')) {

    }
    if(!$user->hasPermission('admin'))
    {
        CHRM_Redirect::to('emp_dashboard.php');
    }
}

if(CHRM_Input::exists()) {



    if(CHRM_Token::check(CHRM_Input::get('token'))) {
        // CHRM_Input::get('username');
        $validate = new CHRM_Validation();
        $validation = $validate->check($_POST, array(
            'username' => array(
                'required' => true,
                'min' => 1,
                'max' => 20,
                //'unique' => 'users'
            ),
            'password' => array(
                'required' => true,
                'min' => 1,
                'max' => 20
            ),
            'password_again' => array(
                'required' => true,
                'matches' => 'password'
            ),
            'empid' => array(
                'required' => true,
                'min' => 1,
                'max' => 50
            )
        ));

        $displayval='display-hide';

        if($validation->passed()) {
            $user = new CHRM_User();
            $salt = CHRM_Hash::salt(32);


            try {
                $user->create(array(
                    'username' => CHRM_Input::get('username'),
                    'password' => CHRM_Hash::make(CHRM_Input::get('password'), $salt),
                    'salt' => $salt,
                    'empid' => CHRM_Input::get('empid'),
                    'group' => CHRM_Input::get('privilage')
                ));

                $displayval = '';
                //CHRM_Redirect::to('adduser.php');

            } catch (Exception $e) {
                die($e->getMessage());
            }
        } else {
            foreach($validation->errors() as $error) {
                echo $error, '<br>';
            }
        }
    }
}
?>
<!DOCTYPE html>


<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>CoffeeHRM | Attendance </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="Osama Iqbal" name="author"/>




    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->






    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <script src="assets/plugins/excanvas.min.js"></script>
    <![endif]-->


    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
    <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/tableExport.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
    <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
    <script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="assets/scripts/core/app.js"></script>
    <script src="assets/scripts/custom/table-editable.js"></script>



    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>


    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2-metronic.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css">
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="css/style-coffeehrm.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="favicon.ico"/>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">

<!-- BEGIN HEADER -->
<div class="header navbar navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="header-inner">
        <!-- BEGIN LOGO -->
        <a class="navbar-brand" href="dashboard.php">
            <img src="assets/img/logo.png" alt="logo" class="img-responsive"/>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <img src="assets/img/menu-toggler.png" alt=""/>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <ul class="nav navbar-nav pull-right">


            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

					<span class="username">
						 Welcome
					</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu">


                    </li>
                    <li>
                        <a href="javascript:;" id="trigger_fullscreen">
                            <i class="fa fa-arrows"></i> Full Screen
                        </a>
                    </li>
            <li class="divider">
                    <li>
                        <a href="logout.php">
                            <i class="fa fa-key"></i> Log Out
                        </a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>

            <li class = "start">
                <a href="companystructure.php">
                    <i class="fa fa-sitemap"></i>
						<span class="title">
							Company Structure
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
            <li>
                <a href="monitorattendance.php">
                    <i class="fa fa-check-circle-o"></i>
						<span class="title">
							Monitor Attendance
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li>
                <a href="employees.php">
                    <i class="fa fa-user"></i>
						<span class="title">
							Employees
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li >
                <a href="jobs.php">
                    <i class="fa fa-rupee"></i>
						<span class="title">
							Jobs
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li >
                <a href="qualifications.php">
                    <i class="fa fa-cogs"></i>
						<span class="title">
							Qualifications
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li >
                <a href="documentmanagement.php">
                    <i class="fa fa-files-o"></i>
						<span class="title">
							Document Management
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li >
                <a href="leaves.php">
                    <i class="fa fa-calendar"></i>
						<span class="title">
							Leaves
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>

            <li >
                <a href="clients.php">
                    <i class="fa fa-users"></i>
						<span class="title">
							Clients
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
            <li>
                <a href="projects.php">
                    <i class="fa fa-clock-o"></i>
						<span class="title">
							Projects
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>
            <li class = "last">
                <a href="adduser.php">
                    <i class="fa fa-male"></i>
						<span class="title">
							Add User
						</span>
						<span class="arrow ">
						</span>
                </a>
            </li>




            <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Add User <small>Add new user</small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">

            <li>
                <i class="fa fa-home"></i>
                <a href="dashboard.php">
                    Home
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">
                    Admin
                </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="adduser.php">
                    Add New User
                </a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">

<div class="portlet box blue">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-edit"></i>Add New User
    </div>
    <div class="tools">
        <a href="javascript:;" class="collapse">
        </a>
        <a href="javascript:;" class="reload">
        </a>
        <a href="javascript:;" class="remove">
        </a>
    </div>
</div>
<div class="portlet-body">
<div class="table-toolbar">

</div>
    <form id="form_sample_1" class="form-horizontal empadd" name="empadd" action="" method="post">
        <div class="form-body">
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                Values have not been updated. You have some form errors. Please check below.
            </div>
            <div class="alert alert-success <?php if(isset($_POST['username'])) {echo '';} else {echo 'display-hide';} ?>">
                <button class="close" data-close="alert"></button>
                Values have been added sucessfully.

            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Employee Username
										<span class="required">
											 *
										</span>
                </label>
                <div class="col-md-4">
                    <input required type="text" id="username" name="username" autocomplete="off" value="" class="form-control input-large">


                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Password
										<span class="required">
											 *
										</span>
                </label>
                <div class="col-md-4">
                    <input required type="password" name="password" data-required="1" autocomplete="off" class="form-control input-large" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Enter Password Again
    <span class="required">
											 *
										</span>
                </label>

                <div class="col-md-4">
                    <input type="password" id="password_again" name="password_again" value="" class="form-control input-large"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Enter ID
										<span class="required">
											 *
										</span>
                </label>
                <div class="col-md-4">
                    <select required name = "empid" data-required="1"  class="form-control input-large">
                        <option value = "">Select One</option>
                        <?php
                        $server = mysql_connect('localhost', 'root', '');
                        $db =  mysql_select_db('CoffeeHRM',$server);
                        $query = mysql_query("select * from employees");
                        //$i = 1;
                        while ($row = mysql_fetch_array($query)) {
                            echo '<option value = ',$row['id'] , '>', $row['firstname'], '</option>';
                        }
                        //$i++;
                        ?>

                    </select>
                    <input type = "hidden" name = "token" value = "<?php echo CHRM_Token::generate()?>">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3">Employee Privilages
										<span class="required">
											 *
										</span>
                </label>
                <div class="col-md-4">
                    <select required name = "privilage" data-required="1"  class="form-control input-large">
                        <option value = "">Select One</option>
                        <option value = "1">Employee</option>
                        <option value = "2">Administrator</option>

                    </select>

                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">

                </label>
                <div class="col-md-4">
                <input type="submit" class="btn btn-success" value="Add User" />
                    </div>
            </div>

        </div>


<!-- END PAGE CONTENT -->
</div>
</div>
<!-- END CONTENT -->
</div></div></div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2014 &copy; CoffeeHRM by Osama Iqbal and Chirag Parasrampuria.

    </div>
    <div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
    </div>
</div>
<!-- END FOOTER -->


<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/respond.min.js"></script>
<script src="assets/plugins/excanvas.min.js"></script>
<![endif]-->


<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>

<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
        TableEditable.init();


    });
</script>


<script src="assets/scripts/custom/table-editable.js"></script>



<!--<script>
    $(function(){
        $('#myFormSubmit').click(function(e){
            e.preventDefault();
            alert($('#abc').val());
            /*
             $.post('http://path/to/post',
             $('#myForm').serialize(),
             function(data, status, xhr){
             // do something here with response;
             });
             */
        });
    });
</script> -->

<!-- BEGIN PAGE LEVEL STYLES -->






<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->





<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/tableExport.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.base64.js"></script>
<script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/jspdf.js"></script>
<script type="text/javascript" src="assets/plugins/jspdf/libs/base64.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-markdown/lib/markdown.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/table-editable.js"></script>






<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/components-pickers.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script src="assets/scripts/core/app.js"></script>
<script src="assets/scripts/custom/form-validation.js"></script>
<!-- END PAGE LEVEL STYLES -->


</body>
<!-- END BODY -->
</html>


