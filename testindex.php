<?php

    require_once 'inc/core/init.php';

    if(CHRM_Session::exists('home')) {
        echo CHRM_Session::flash('home');
    }

    $user = new CHRM_User();
    //echo $user->data()->username;

    if($user->isLoggedIn())
    {
        print_r($_SESSION);
        echo '<br>', $user->data()->username;

?>

        <p>Hello, <a href="profile.php?user=<?php echo escape($user->data()->username); ?>"><?php echo escape($user->data()->username); ?></a> ! </p>

        <ul>
            <li><a href="logout.php">Logout</a></li>
            <li><a href="update.php">Update Details</a></li>
            <li><a href="changepassword.php">Change Password</a></li

        </ul>
<?php
        if($user->hasPermission('admin')) {
            echo '<p> You are an Admin </p>';
        }
        if(!$user->hasPermission('admin'))
        {
            echo '<p> You are an employee </p>';
        }

        //echo 'Logged In!';
    } else {
        echo '<p>You need to <a href="login.php">login</a> or <a href="testregister.php">register</a>';
    }