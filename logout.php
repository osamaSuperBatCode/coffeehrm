<?php

    require_once 'inc/core/init.php';

    $user = new CHRM_User();
    $user->logout();

    CHRM_Redirect::to('index.php');